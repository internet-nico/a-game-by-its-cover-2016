﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinSpawner : MonoBehaviour, IEventListener {

	public GameObject prefab;
	public GameObject container;

	private List<GameObject> coins = new List<GameObject>();
	private float currentX = 0f;
	private float currentY = 32f;
	private int coinIndex = 0;

	public List<GameObject> GetCoins() {
		return coins;
	}
	public GameObject GetCoinAt(int index) {
		if (index >= 0 && index <= coins.Count - 1) {
			return coins [index];
		}
		return null;
	}

	// Use this for initialization
	void Start () {
		//		StartCoroutine (SpawnWithHeight());

		// Listen for coin expiring
//		EventManager.instance.AddListener(this as IEventListener, "CoinExpiredEvent");
	}

	// Update is called once per frame
	//	void Update () {
	//		
	//	}

	bool IEventListener.HandleEvent(IEvent evt)
	{
		switch (evt.GetName ()) {
		case "CoinExpiredEvent": 
			GameObject coin = (GameObject)evt.GetData ();
			coins.Remove (coin);
			Destroy (coin);
			break;
		}

		return true;
	}

	public void SpawnLevel(string[] levelLines) {
		StartCoroutine (SpawnLevelCoins (levelLines));
	}

	IEnumerator SpawnLevelCoins(string[] levelLines) {
		for (int i = 0; i < levelLines.Length; i++) {
			string type = levelLines [i];

			//			Debug.Log ("making: " + type);

			if (type == "0") {

				// "Empty" coin
				coins.Add (null);

				// Skip one...
				currentX += 16;
				currentY += 8;

				// Increment index
				coinIndex++;

				// Don't wait.
				continue;

			} else if (type == "1") {
				// Set position
				Vector3 pos = new Vector3 (currentX, currentY, 0);
				Debug.Log ("spawning coin @ " + pos);
				// Instantiate coin prefab
				GameObject coin = (GameObject)Instantiate(prefab, pos, Quaternion.identity);
				coins.Add (coin);
				// Apply sorting order
				coin.GetComponent<SpriteRenderer>().sortingOrder = 999 - coinIndex*2;
				// Add to container
				coin.transform.parent = container.transform;
				// Update position for next time
				currentX += 16;
				currentY += 8;
				// Increment index
				coinIndex++;
				// Dispatch event
//				EventManager.instance.QueueEvent(new CoinSpawnedEvent());
			}

			yield return new WaitForSeconds (1f);
		}
	}

}
