﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class LevelLoader: MonoBehaviour {

	private TextAsset levelFile;
	private string[] levelLines;
	private TextAsset coinFile;
	private string[] coinLines;

	private static LevelLoader levelLoader;

	public static string[] GetLevelLines() {
		return instance.levelLines;
	}

	public static string[] GetCoinLines() {
		return instance.coinLines;
	}

	public static LevelLoader instance
	{
		get
		{
			if (!levelLoader)
			{
				levelLoader = FindObjectOfType (typeof (LevelLoader)) as LevelLoader;

				if (!levelLoader)
				{
					Debug.LogError ("There needs to be one active LevelLoader script on a GameObject in your scene.");
				}
				else
				{
					levelLoader.Init (); 
				}
			}

			return levelLoader;
		}
	}

	void Init ()
	{
		
	}

	public static void LoadLevel(int level) {
		// Get the associated textAsset
		instance.levelFile = (TextAsset) Resources.Load ("Level"+ level);

		if( instance.levelFile != null ) {
			instance.levelLines = instance.levelFile.text.Split("\n".ToCharArray());
		}

		instance.coinFile = (TextAsset) Resources.Load ("Coin" + level);

		if( instance.coinFile != null ) {
			instance.coinLines = instance.coinFile.text.Split("\n".ToCharArray());
		}

	}
}