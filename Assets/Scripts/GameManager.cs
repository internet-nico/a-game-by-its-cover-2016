﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour, IEventListener {

	private static string[] COIN_SOUNDS = new string[] {
		"0,.07,,.15,.3526,.4,.3,.37,,,,,,,,,.5,.5389,,,,,,,,1,,,,,,",
		"0,.07,,.15,.3526,.4,.3,.35,,,,,,,,,.5,.5389,,,,,,,,1,,,,,,",
		"0,.07,,.15,.3526,.4,.3,.33,,,,,,,,,.5,.5389,,,,,,,,1,,,,,,"
	};
	private static string[] HURT_SOUNDS = new string[] {
		"3,.09,,.16,,.2573,.3,.4134,,-.6825,,,,,,,,,,,,,,,,1,,,,,,",
		"3,.09,,.15,,.2573,.3,.48,,-.88,,,,,,,,,,,,,,,,1,,,,,,"
	};

	public GameObject mainCamera;
	public GameObject player;
	public GameObject death;
	public GameObject blockContainer;

	private SfxrSynth synth;
	private BlockSpawner blockSpawner;
	private CoinSpawner coinSpawner;
	private bool isPaused = false;
	private bool isDead = false;
	private int currentLevel = 1;

	public bool GetIsPaused() {
		return isPaused;
	}
	public bool GetIsDead() {
		return isDead;
	}

	// Use this for initialization
	void Start () {
		// Get components
		synth = new SfxrSynth();
		blockSpawner = GetComponent<BlockSpawner> ();
		coinSpawner = GetComponent<CoinSpawner> ();

		// Set new camera target
		SmoothCamera2D smoothCamera = (SmoothCamera2D)mainCamera.GetComponent<SmoothCamera2D> ();
		smoothCamera.target = player.transform;

		// Load the current level
		LevelLoader.LoadLevel(currentLevel);

		// Spawn blocks
		blockSpawner.SpawnLevel(LevelLoader.GetLevelLines ());

		// Spawn coins
		coinSpawner.SpawnLevel(LevelLoader.GetCoinLines ());

		// Listen for block spawned event
		EventManager.instance.AddListener(this as IEventListener, "BlockSpawnedEvent");
		// Listen for player movement
		EventManager.instance.AddListener(this as IEventListener, "PlayerMoveStartEvent");
		EventManager.instance.AddListener(this as IEventListener, "PlayerMoveEndEvent");
		// Listen for coin pickups
		EventManager.instance.AddListener(this as IEventListener, "PlayerPickedUpCoinEvent");
		// Listen for death
		EventManager.instance.AddListener(this as IEventListener, "DeathEvent");
	}

	// Update is called once per frame
	void Update () {
		// Escape key
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (isPaused) {
				// Resume the game
				ResumeGame();
				// Close the menu

			} else {
				// Pause the game
				PauseGame();
				// Display a menu

			}
		}

		if (isDead && Input.GetKeyDown (KeyCode.Space)) {
			RestartLevel ();
		}
	}

	bool IEventListener.HandleEvent(IEvent evt)
	{
		switch (evt.GetName ()) {
		case "BlockSpawnedEvent": 
			BlockSpawnedHandler ();
			break;
		case "PlayerMoveStartEvent": 
			PlayerMoveStartHandler ();
			break;
		case "PlayerMoveEndEvent": 
			PlayerMoveEndHandler ();
			break;
		case "PlayerPickedUpCoinEvent":
			PlayerPickedUpCoinHandler ();
			break;
		case "DeathEvent":
			PlayerDiedHandler ();
			break;
		}

//		string txt = evt.GetData() as string;
//		Debug.Log("Event received: " + evt.GetName() + " with data: " + txt);

		return true;
	}

	void PlayerMoveStartHandler() {
		// Move death position
		Vector3 deathPos = death.gameObject.transform.position;
		deathPos.x = player.gameObject.transform.position.x;
		deathPos.y = player.gameObject.transform.position.y - 200;
		death.transform.position = deathPos;
	}

	void PlayerMoveEndHandler() {
		// Get the player components
		SpriteRenderer playerRenderer = player.GetComponent<SpriteRenderer> ();
		PlayerController playerController = player.GetComponent<PlayerController> ();

		// Fix player sorting order
		if (playerController.moveIndex > 0) {
			int blockOrder = 999 - playerController.moveIndex * 2;

//			Debug.Log ("changing player order");
			// If player is falling...
			if (!playerController.isMoving && player.gameObject.transform.position.y < (16 + playerController.moveIndex * 8 - 1)) {
				// Move the player behind the previous block
				playerRenderer.sortingOrder = blockOrder - 1;
			} else {
				// Keep the player above the blocks
				playerRenderer.sortingOrder = blockOrder + 1;
			}
		}
	}

	void PlayerPickedUpCoinHandler() {
		var sound = COIN_SOUNDS[Random.Range(0, COIN_SOUNDS.Length-1)];
		synth.parameters.SetSettingsString(sound);
		synth.Play();
	}

	void PlayerDiedHandler() {
		// Play sound
		var sound = HURT_SOUNDS[Random.Range(0, HURT_SOUNDS.Length-1)];
		synth.parameters.SetSettingsString(sound);
		synth.Play();

		// Pause game
		GameOver ();
	}

	void BlockSpawnedHandler() {
//		Debug.Log ("created!");
		if (blockSpawner.currentBlock != null) {
			SmoothCamera2D smoothCamera = (SmoothCamera2D)mainCamera.GetComponent<SmoothCamera2D> ();
			
			// Set new camera target
			smoothCamera.target = blockSpawner.currentBlock.gameObject.transform;
		}
	}

	void PauseGame() {
		Debug.Log ("Pause.");
		Time.timeScale = 0f;
		isPaused = true;
	}

	void ResumeGame() {
		Debug.Log ("Resume.");
		Time.timeScale = 1f;
		isPaused = false;
	}

	void GameOver() {
		Debug.Log ("Dead.");
		Time.timeScale = 0f;
		isPaused = true;
		isDead = true;
	}

	void RestartLevel() {
		SceneManager.LoadScene ("Level1");
		isDead = false;
		ResumeGame ();

		// Dispatch event
		EventManager.instance.QueueEvent (new RestartEvent ());
	}

	int GetSortingOrderOfBlockAt(int index) {
		var block = blockSpawner.GetBlockAt (index);
		if (block != null) {
			return block.GetComponent<SpriteRenderer> ().sortingOrder;
		}

		return 999;
	}
}
