﻿using UnityEngine;
using System.Collections;

public class ColorChanger : MonoBehaviour {

	private const float INCREMENT = 0.1f;
	private const float WAIT = .2f;

	public bool enabled = false;

	private Color fromColor;
	private Color toColor;

	// Use this for initialization
	void Start () {
		this.enabled = true;

		fromColor = new Color(48/255f, 149/255f, 245/255f);
		toColor = new Color(13/255f, 64/255f, 143/255f);
		StartCoroutine (Fade(fromColor, toColor));
	}

//	
//	// Update is called once per frame
//	void Update () {
//		
//	}

	void FadeCompleted () {
		// Swap variables
		Color temp = fromColor;
		fromColor = toColor;
		toColor = temp;

		// Restart
		StartCoroutine (Fade (fromColor, toColor));
	}

	IEnumerator Fade(Color startColor, Color endColor) {

		for (float f = 0f; f < 1f; f += INCREMENT) {
			Color c = Color.Lerp(startColor, endColor, f);
			
			// Change renderer color
			gameObject.GetComponent<SpriteRenderer>().color = c;
			
			// Wait
			yield return new WaitForSeconds(WAIT);
		}

		// Dispatch "event"
		FadeCompleted();
	}
}
