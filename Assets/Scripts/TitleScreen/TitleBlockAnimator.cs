﻿using UnityEngine;
using System.Collections;

public class TitleBlockAnimator : MonoBehaviour {

	public float waitTime = 0.75f;
	public float duration = 1.0f;

	private SpriteRenderer spriteRenderer; 

	// Use this for initialization
	void Start () {
		// Get Components
		spriteRenderer = GetComponent<SpriteRenderer>();

		// Start alpha at 0
		var colorStart = spriteRenderer.color;
		colorStart.a = 0.0f;
		spriteRenderer.color = colorStart;

		// Start animations in a few seconds
		Invoke("StartAnimations", waitTime);
	}

	void StartAnimations() {
		// Start FadeIn
		StartCoroutine (HelperMethods.FadeIn (spriteRenderer, duration));

		// Start bob motion
		StartCoroutine (BobSprite ());
	}

	public float speed = 125f;
	public float maxHeight = 7f;
	public float offsetY = 0f;

	private float angle = 0f;
	private float toDeg = Mathf.PI/180;

	IEnumerator BobSprite() {
		while (true) {
			angle += speed * Time.deltaTime;
			if (angle > 360)
				angle -= 360;

			var position = gameObject.transform.position;
			position.y = maxHeight * Mathf.Sin (angle * toDeg) + offsetY;
			gameObject.transform.position = position;

			yield return null;
		}
	}
}
