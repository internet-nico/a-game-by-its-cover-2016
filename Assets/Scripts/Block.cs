﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	private SpriteRenderer spriteRenderer;

	public int order;

	private GameManager gameManager;
	private int life = 60*4; // ~4 seconds

	// Use this for initialization
	void Start () {
		// Get components
		GameObject game = GameObject.Find("Game");
		gameManager = game.GetComponent<GameManager>();
		spriteRenderer = GetComponent<SpriteRenderer>();

		// Start life cycle
		StartCoroutine(LifeTick());
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}

	IEnumerator LifeTick() {
		while (life > 0) {

			while (gameManager.GetIsPaused ())
			{
				yield return null;
			}
		
			life--;

			yield return null;
		}

		// Blink countdown
		StartCoroutine (DoBlinks (0.2f, 0.2f));
	}

	IEnumerator DoBlinks(float duration, float blinkTime) {
		while (duration > 0f) {
		
			while (gameManager.GetIsPaused ())
			{
				yield return null;
			}

			duration -= Time.deltaTime;

			// toggle renderer
			spriteRenderer.enabled = !spriteRenderer.enabled;

			// wait for a bit
			yield return new WaitForSeconds(blinkTime);
		}
		// make sure renderer is disabled when we exit
		spriteRenderer.enabled = false;
		
		// Dead. Dispatch event
		EventManager.instance.QueueEvent (new BlockExpiredEvent (gameObject));
	}
}
