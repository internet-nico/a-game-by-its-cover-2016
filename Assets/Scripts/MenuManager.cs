﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour, IEventListener {

	public GameObject canvasObject;

	private Canvas canvas;

	// Use this for initialization
	void Start () {
		// Get components
		canvas = canvasObject.GetComponent<Canvas>();

		// Listen for events
		EventManager.instance.AddListener(this as IEventListener, "DeathMenuEvent");
		EventManager.instance.AddListener(this as IEventListener, "RestartEvent");
	}
	
	// Update is called once per frame
//	void Update () {
//	
//	}

	bool IEventListener.HandleEvent(IEvent evt)
	{
		switch (evt.GetName ()) {
		case "DeathMenuEvent": 
			DeathMenuEventHandler ();
			break;
		case "RestartEvent": 
			RestartEventHandler ();
			break;
		}

		//		string txt = evt.GetData() as string;
		//		Debug.Log("Event received: " + evt.GetName() + " with data: " + txt);

		return true;
	}

	void DeathMenuEventHandler() {
		// Display death screen
		canvas.enabled = true;
	}

	void RestartEventHandler() {
		// Hide everything
		canvas.enabled = false;
	}
}
