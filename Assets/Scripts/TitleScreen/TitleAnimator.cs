﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleAnimator : MonoBehaviour {

	public float waitTime = 2.0f;
	public float duration = 1.0f;

	private Text theText;

	// Use this for initialization
	void Start () {
		// Get components
		theText = GetComponent<Text>();

		// Start alpha at 0
		var colorStart = theText.color;
		colorStart.a = 0.0f;
		theText.color = colorStart;

		// Start animations in a few seconds
		Invoke("StartAnimations", waitTime);
	}

	void StartAnimations() {
		// Start FadeIn
		StartCoroutine (HelperMethods.FadeIn (theText, duration));
	}
}
