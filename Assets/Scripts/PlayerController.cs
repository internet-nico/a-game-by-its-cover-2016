﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private static string[] JUMP_SOUNDS = new string[] {
		"0,.1,,.1013,,.2115,.3,.25,,.226,,,,,,,,,,,.2797,,,,,.6961,,,.0475,,,",
		"0,.1,,.1013,,.2115,.3,.27,,.226,,,,,,,,,,,.2797,,,,,.6961,,,.0475,,,",
		"0,.1,,.1013,,.2115,.3,.29,,.226,,,,,,,,,,,.2797,,,,,.6961,,,.0475,,,"
	};
	private static int JUMP_HEIGHT = 32;

	public GameObject game;

	public float smoothTime = 0.3F;
	public int moveIndex = 0;
	public bool isMoving = false;
	public int lastBlockTouched = 0;

	private SfxrSynth synth;
	private GameManager gameManager;
	private Coroutine currentMoveCoroutine = null;
	private bool isDoubleJumping = false;
	private bool isTripleJumping = false;

	// Use this for initialization
	void Start () {
		gameManager = game.GetComponent<GameManager> ();
		synth = new SfxrSynth();
//		StartCoroutine (AdvanceGame ());
	}
	
	// Update is called once per frame
	void Update () {
		if (gameManager.GetIsDead ())
			return;

		if (!isMoving && Input.GetKeyDown (KeyCode.Space)) {
			// Jump
//			Debug.Log("jump");
			// Increment position
			moveIndex++;
			// Start a jump Coroutine
			currentMoveCoroutine = StartCoroutine (MovePlayerTo (moveIndex, 0.5f));
		} else if (!isDoubleJumping && isMoving && Input.GetKeyDown (KeyCode.Space)) {
			// Double jump!
			isDoubleJumping = true;
			// Stop the current move Coroutine
			StopCoroutine (currentMoveCoroutine);
			// Increment position
			moveIndex++;
			// Start a new one
			currentMoveCoroutine = StartCoroutine (MovePlayerTo (moveIndex, 0.65f));
		} else if (isDoubleJumping && !isTripleJumping && isMoving && Input.GetKeyDown (KeyCode.Space)) {
			// Triple jump!
			isTripleJumping = true;
			// Stop the current move Coroutine
			StopCoroutine (currentMoveCoroutine);
			// Increment position
			moveIndex++;
			// Start a new one
			currentMoveCoroutine = StartCoroutine (MovePlayerTo (moveIndex, 0.65f));
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Death") {
			// Dispatch event on death
			EventManager.instance.QueueEvent (new DeathEvent ());
			// Dispatch show death menu
			EventManager.instance.QueueEvent (new DeathMenuEvent ());
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Block") {
			lastBlockTouched = coll.gameObject.GetComponent<Block> ().order;

//			Debug.Log("LANDED");
			isMoving = false;
			isDoubleJumping = false;
			isTripleJumping = false;
		}
	}

	IEnumerator MovePlayerTo(int index, float time) {
//		Debug.Log ("Start move to " + moveIndex);

		float elapsedTime = 0;
		// Player current position
		Vector3 startPosition = gameObject.transform.position;
		// End position based on current index
		Vector3 endPosition = new Vector3 ((index-1) * 16, 16 + (index-1) * 8, 0f);
		endPosition.x += 16;
		endPosition.y += 8;

		isMoving = true;

		// Dispatch start event
		EventManager.instance.QueueEvent(new PlayerMoveStartEvent());

		// Play sound
		var sound = JUMP_SOUNDS[Random.Range(0, JUMP_SOUNDS.Length-1)];
		synth.parameters.SetSettingsString(sound);
		synth.Play();

		while (elapsedTime < time)
		{
			// calculate straight-line lerp position:
			Vector3 newPosition = Vector3.Lerp(startPosition, endPosition, (elapsedTime / time));
//			transform.position = Vector3.Slerp(startPosition, endPosition, (elapsedTime / time));
			// add a value to Y, using Sine to give a curved trajectory in the Y direction
			newPosition.y += JUMP_HEIGHT * Mathf.Sin(Mathf.Clamp01(elapsedTime / time) * Mathf.PI);
			elapsedTime += Time.deltaTime;
			// finally assign the computed position to our gameObject:
			gameObject.transform.position = newPosition;

//			Debug.Log ("position: " + newPosition);

			yield return null;
		}

		// Finish
		gameObject.transform.position = endPosition;

		// Dispatch end event
		EventManager.instance.QueueEvent(new PlayerMoveEndEvent());

//		Debug.Log ("endPosition: " + endPosition);
	}

	IEnumerator AdvanceGame () {

		while (true) {
			
			StartCoroutine (MovePlayerTo (moveIndex, 0.25f));
			moveIndex++;

			yield return new WaitForSeconds (1);
		}
	}
}
