﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockSpawner : MonoBehaviour, IEventListener {

	public GameObject prefab;
	public GameObject container;
	public GameObject currentBlock;

	private List<GameObject> blocks = new List<GameObject>();
	private float currentX = 0f;
	private float currentY = 0f;
	private int blockIndex = 0;

	public List<GameObject> GetBlocks() {
		return blocks;
	}
	public GameObject GetBlockAt(int index) {
		if (index >= 0 && index <= blocks.Count - 1) {
			return blocks [index];
		}
		return null;
	}

	// Use this for initialization
	void Start () {
//		StartCoroutine (SpawnWithHeight());

		// Listen for block expiring
		EventManager.instance.AddListener(this as IEventListener, "BlockExpiredEvent");
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}

	bool IEventListener.HandleEvent(IEvent evt)
	{
		switch (evt.GetName ()) {
		case "BlockExpiredEvent": 
			GameObject block = (GameObject)evt.GetData ();
			blocks.Remove (block);
			Destroy (block);
			break;
		}

		return true;
	}

	public void SpawnLevel(string[] levelLines) {
		StartCoroutine (SpawnLevelBlocks (levelLines));
	}

	IEnumerator SpawnLevelBlocks(string[] levelLines) {
		for (int i = 0; i < levelLines.Length; i++) {
			string type = levelLines [i];

//			Debug.Log ("making: " + type);

			if (type == "0") {

				// "Empty" block
				blocks.Add (null);

				// Skip one...
				currentX += 16;
				currentY += 8;

				// Increment index
				blockIndex++;

				// Don't wait.
				continue;

			} else if (type == "1") {
				// Set position
				Vector3 pos = new Vector3 (currentX, currentY, 0);
				// Instantiate block prefab
				GameObject block = (GameObject)Instantiate(prefab, pos, Quaternion.identity);
				blocks.Add (block);
				// Apply sorting order
				block.GetComponent<SpriteRenderer>().sortingOrder = 999 - blockIndex*2;
				// Set sorting order as property
				block.GetComponent<Block>().order = blockIndex;
				// Add to container
				block.transform.parent = container.transform;
				// Update position for next time
				currentX += 16;
				currentY += 8;
				// Set current block
				currentBlock = block;
				// Increment index
				blockIndex++;
				// Dispatch event
				EventManager.instance.QueueEvent(new BlockSpawnedEvent());
			}

			yield return new WaitForSeconds (1f);
		}
	}

	// Test heights
	IEnumerator SpawnWithHeight() {
		int lastColumnHeight = 0;
		int currentOrder = 1000;

		for (int i = 0; i < 10; i++) {
			// Set position
			Vector3 pos = new Vector3 (currentX, currentY, 0);
			// Random height
			int columnHeight = lastColumnHeight != 0 ? lastColumnHeight + Random.Range(0,3) : 1;
			// Column of blocks
			float tempY = currentY;
			// Get starting order of this column
			currentOrder = currentOrder - lastColumnHeight - columnHeight + 1;
			// Create a column of blocks
			for (int j = 0; j < columnHeight; j++) {
				// Update position from loop updates
				pos.x = currentX;
				pos.y = currentY;
				// Instantiate block prefab
				GameObject block = (GameObject)Instantiate(prefab, pos, Quaternion.identity);
				// Increment sorting order for each block
				block.GetComponent<SpriteRenderer> ().sortingOrder = currentOrder + j;
				// Add to container
				block.transform.parent = container.transform;
				// Update position for same column
				currentY += 16;
				pos.y = currentY;
				// Set current block
				currentBlock = block;
			}
			// Update position for next time
//			if (Random.value > 0.5) {
				currentX += 16;
//			} else {
//				currentX -= 16;
//			}
				
			currentY = tempY + 8; // Back to original Y
			currentOrder += columnHeight - 1;
			lastColumnHeight = columnHeight;
			// Dispatch event
//			EventManager.TriggerEvent ("blockSpawned");
			// Wait...
			yield return new WaitForSeconds (0.3f);
		}
	}
}
