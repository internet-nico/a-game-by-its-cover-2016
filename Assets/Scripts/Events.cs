﻿using UnityEngine;
using UnityEngine.Events;

public class BlockSpawnedEvent : IEvent
{
	public BlockSpawnedEvent()
	{
		
	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "BlockSpawnedEvent Data goes here!";
	}
}

public class BlockExpiredEvent : IEvent
{
	private GameObject block;

	public BlockExpiredEvent(GameObject block)
	{
		this.block = block;
	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return block;
	}
}

public class PlayerMoveStartEvent : IEvent
{
	public PlayerMoveStartEvent()
	{

	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "PlayerMoveStartEvent Data goes here!";
	}
}

public class PlayerMoveEndEvent : IEvent
{
	public PlayerMoveEndEvent()
	{
		
	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "PlayerMoveEndEvent Data goes here!";
	}
}

public class PlayerPickedUpCoinEvent : IEvent
{
	public PlayerPickedUpCoinEvent()
	{

	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "PlayerPickedUpCoinEvent Data goes here!";
	}
}

public class DeathEvent : IEvent
{
	public DeathEvent()
	{
		
	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "DeathEvent Data goes here!";
	}
}

public class DeathMenuEvent : IEvent
{
	public DeathMenuEvent()
	{
		
	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "DeathMenuEvent Data goes here!";
	}
}

public class RestartEvent : IEvent
{
	public RestartEvent()
	{

	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "DeathEvent Data goes here!";
	}
}